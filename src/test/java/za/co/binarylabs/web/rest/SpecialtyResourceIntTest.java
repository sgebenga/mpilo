package za.co.binarylabs.web.rest;

import za.co.binarylabs.Application;
import za.co.binarylabs.domain.Specialty;
import za.co.binarylabs.repository.SpecialtyRepository;
import za.co.binarylabs.repository.search.SpecialtySearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SpecialtyResource REST controller.
 *
 * @see SpecialtyResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SpecialtyResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private SpecialtyRepository specialtyRepository;

    @Inject
    private SpecialtySearchRepository specialtySearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSpecialtyMockMvc;

    private Specialty specialty;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SpecialtyResource specialtyResource = new SpecialtyResource();
        ReflectionTestUtils.setField(specialtyResource, "specialtySearchRepository", specialtySearchRepository);
        ReflectionTestUtils.setField(specialtyResource, "specialtyRepository", specialtyRepository);
        this.restSpecialtyMockMvc = MockMvcBuilders.standaloneSetup(specialtyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        specialty = new Specialty();
        specialty.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createSpecialty() throws Exception {
        int databaseSizeBeforeCreate = specialtyRepository.findAll().size();

        // Create the Specialty

        restSpecialtyMockMvc.perform(post("/api/specialtys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(specialty)))
                .andExpect(status().isCreated());

        // Validate the Specialty in the database
        List<Specialty> specialtys = specialtyRepository.findAll();
        assertThat(specialtys).hasSize(databaseSizeBeforeCreate + 1);
        Specialty testSpecialty = specialtys.get(specialtys.size() - 1);
        assertThat(testSpecialty.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = specialtyRepository.findAll().size();
        // set the field null
        specialty.setName(null);

        // Create the Specialty, which fails.

        restSpecialtyMockMvc.perform(post("/api/specialtys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(specialty)))
                .andExpect(status().isBadRequest());

        List<Specialty> specialtys = specialtyRepository.findAll();
        assertThat(specialtys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpecialtys() throws Exception {
        // Initialize the database
        specialtyRepository.saveAndFlush(specialty);

        // Get all the specialtys
        restSpecialtyMockMvc.perform(get("/api/specialtys?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(specialty.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSpecialty() throws Exception {
        // Initialize the database
        specialtyRepository.saveAndFlush(specialty);

        // Get the specialty
        restSpecialtyMockMvc.perform(get("/api/specialtys/{id}", specialty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(specialty.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSpecialty() throws Exception {
        // Get the specialty
        restSpecialtyMockMvc.perform(get("/api/specialtys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpecialty() throws Exception {
        // Initialize the database
        specialtyRepository.saveAndFlush(specialty);

		int databaseSizeBeforeUpdate = specialtyRepository.findAll().size();

        // Update the specialty
        specialty.setName(UPDATED_NAME);

        restSpecialtyMockMvc.perform(put("/api/specialtys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(specialty)))
                .andExpect(status().isOk());

        // Validate the Specialty in the database
        List<Specialty> specialtys = specialtyRepository.findAll();
        assertThat(specialtys).hasSize(databaseSizeBeforeUpdate);
        Specialty testSpecialty = specialtys.get(specialtys.size() - 1);
        assertThat(testSpecialty.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteSpecialty() throws Exception {
        // Initialize the database
        specialtyRepository.saveAndFlush(specialty);

		int databaseSizeBeforeDelete = specialtyRepository.findAll().size();

        // Get the specialty
        restSpecialtyMockMvc.perform(delete("/api/specialtys/{id}", specialty.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Specialty> specialtys = specialtyRepository.findAll();
        assertThat(specialtys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
