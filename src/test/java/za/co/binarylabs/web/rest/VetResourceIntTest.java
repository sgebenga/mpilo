package za.co.binarylabs.web.rest;

import za.co.binarylabs.Application;
import za.co.binarylabs.domain.Vet;
import za.co.binarylabs.repository.VetRepository;
import za.co.binarylabs.repository.search.VetSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the VetResource REST controller.
 *
 * @see VetResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class VetResourceIntTest {

    private static final String DEFAULT_FIRST_NAME = "AAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBB";
    private static final String DEFAULT_LAST_NAME = "AAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBB";

    @Inject
    private VetRepository vetRepository;

    @Inject
    private VetSearchRepository vetSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restVetMockMvc;

    private Vet vet;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VetResource vetResource = new VetResource();
        ReflectionTestUtils.setField(vetResource, "vetSearchRepository", vetSearchRepository);
        ReflectionTestUtils.setField(vetResource, "vetRepository", vetRepository);
        this.restVetMockMvc = MockMvcBuilders.standaloneSetup(vetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        vet = new Vet();
        vet.setFirstName(DEFAULT_FIRST_NAME);
        vet.setLastName(DEFAULT_LAST_NAME);
    }

    @Test
    @Transactional
    public void createVet() throws Exception {
        int databaseSizeBeforeCreate = vetRepository.findAll().size();

        // Create the Vet

        restVetMockMvc.perform(post("/api/vets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vet)))
                .andExpect(status().isCreated());

        // Validate the Vet in the database
        List<Vet> vets = vetRepository.findAll();
        assertThat(vets).hasSize(databaseSizeBeforeCreate + 1);
        Vet testVet = vets.get(vets.size() - 1);
        assertThat(testVet.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testVet.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
    }

    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = vetRepository.findAll().size();
        // set the field null
        vet.setFirstName(null);

        // Create the Vet, which fails.

        restVetMockMvc.perform(post("/api/vets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vet)))
                .andExpect(status().isBadRequest());

        List<Vet> vets = vetRepository.findAll();
        assertThat(vets).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = vetRepository.findAll().size();
        // set the field null
        vet.setLastName(null);

        // Create the Vet, which fails.

        restVetMockMvc.perform(post("/api/vets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vet)))
                .andExpect(status().isBadRequest());

        List<Vet> vets = vetRepository.findAll();
        assertThat(vets).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVets() throws Exception {
        // Initialize the database
        vetRepository.saveAndFlush(vet);

        // Get all the vets
        restVetMockMvc.perform(get("/api/vets?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(vet.getId().intValue())))
                .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
                .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())));
    }

    @Test
    @Transactional
    public void getVet() throws Exception {
        // Initialize the database
        vetRepository.saveAndFlush(vet);

        // Get the vet
        restVetMockMvc.perform(get("/api/vets/{id}", vet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(vet.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVet() throws Exception {
        // Get the vet
        restVetMockMvc.perform(get("/api/vets/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVet() throws Exception {
        // Initialize the database
        vetRepository.saveAndFlush(vet);

		int databaseSizeBeforeUpdate = vetRepository.findAll().size();

        // Update the vet
        vet.setFirstName(UPDATED_FIRST_NAME);
        vet.setLastName(UPDATED_LAST_NAME);

        restVetMockMvc.perform(put("/api/vets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vet)))
                .andExpect(status().isOk());

        // Validate the Vet in the database
        List<Vet> vets = vetRepository.findAll();
        assertThat(vets).hasSize(databaseSizeBeforeUpdate);
        Vet testVet = vets.get(vets.size() - 1);
        assertThat(testVet.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testVet.getLastName()).isEqualTo(UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void deleteVet() throws Exception {
        // Initialize the database
        vetRepository.saveAndFlush(vet);

		int databaseSizeBeforeDelete = vetRepository.findAll().size();

        // Get the vet
        restVetMockMvc.perform(delete("/api/vets/{id}", vet.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Vet> vets = vetRepository.findAll();
        assertThat(vets).hasSize(databaseSizeBeforeDelete - 1);
    }
}
