package za.co.binarylabs.web.rest;

import za.co.binarylabs.Application;
import za.co.binarylabs.domain.PetType;
import za.co.binarylabs.repository.PetTypeRepository;
import za.co.binarylabs.repository.search.PetTypeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PetTypeResource REST controller.
 *
 * @see PetTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PetTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AA";
    private static final String UPDATED_NAME = "BB";

    @Inject
    private PetTypeRepository petTypeRepository;

    @Inject
    private PetTypeSearchRepository petTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPetTypeMockMvc;

    private PetType petType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PetTypeResource petTypeResource = new PetTypeResource();
        ReflectionTestUtils.setField(petTypeResource, "petTypeSearchRepository", petTypeSearchRepository);
        ReflectionTestUtils.setField(petTypeResource, "petTypeRepository", petTypeRepository);
        this.restPetTypeMockMvc = MockMvcBuilders.standaloneSetup(petTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        petType = new PetType();
        petType.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createPetType() throws Exception {
        int databaseSizeBeforeCreate = petTypeRepository.findAll().size();

        // Create the PetType

        restPetTypeMockMvc.perform(post("/api/petTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(petType)))
                .andExpect(status().isCreated());

        // Validate the PetType in the database
        List<PetType> petTypes = petTypeRepository.findAll();
        assertThat(petTypes).hasSize(databaseSizeBeforeCreate + 1);
        PetType testPetType = petTypes.get(petTypes.size() - 1);
        assertThat(testPetType.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = petTypeRepository.findAll().size();
        // set the field null
        petType.setName(null);

        // Create the PetType, which fails.

        restPetTypeMockMvc.perform(post("/api/petTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(petType)))
                .andExpect(status().isBadRequest());

        List<PetType> petTypes = petTypeRepository.findAll();
        assertThat(petTypes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPetTypes() throws Exception {
        // Initialize the database
        petTypeRepository.saveAndFlush(petType);

        // Get all the petTypes
        restPetTypeMockMvc.perform(get("/api/petTypes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(petType.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getPetType() throws Exception {
        // Initialize the database
        petTypeRepository.saveAndFlush(petType);

        // Get the petType
        restPetTypeMockMvc.perform(get("/api/petTypes/{id}", petType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(petType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPetType() throws Exception {
        // Get the petType
        restPetTypeMockMvc.perform(get("/api/petTypes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePetType() throws Exception {
        // Initialize the database
        petTypeRepository.saveAndFlush(petType);

		int databaseSizeBeforeUpdate = petTypeRepository.findAll().size();

        // Update the petType
        petType.setName(UPDATED_NAME);

        restPetTypeMockMvc.perform(put("/api/petTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(petType)))
                .andExpect(status().isOk());

        // Validate the PetType in the database
        List<PetType> petTypes = petTypeRepository.findAll();
        assertThat(petTypes).hasSize(databaseSizeBeforeUpdate);
        PetType testPetType = petTypes.get(petTypes.size() - 1);
        assertThat(testPetType.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deletePetType() throws Exception {
        // Initialize the database
        petTypeRepository.saveAndFlush(petType);

		int databaseSizeBeforeDelete = petTypeRepository.findAll().size();

        // Get the petType
        restPetTypeMockMvc.perform(delete("/api/petTypes/{id}", petType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PetType> petTypes = petTypeRepository.findAll();
        assertThat(petTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
