'use strict';

describe('Pet Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockPet, MockPetType, MockUser, MockVisit;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockPet = jasmine.createSpy('MockPet');
        MockPetType = jasmine.createSpy('MockPetType');
        MockUser = jasmine.createSpy('MockUser');
        MockVisit = jasmine.createSpy('MockVisit');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Pet': MockPet,
            'PetType': MockPetType,
            'User': MockUser,
            'Visit': MockVisit
        };
        createController = function() {
            $injector.get('$controller')("PetDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'mpiloApp:petUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
