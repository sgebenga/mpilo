'use strict';

describe('Visit Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockVisit, MockPet;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockVisit = jasmine.createSpy('MockVisit');
        MockPet = jasmine.createSpy('MockPet');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Visit': MockVisit,
            'Pet': MockPet
        };
        createController = function() {
            $injector.get('$controller')("VisitDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'mpiloApp:visitUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
