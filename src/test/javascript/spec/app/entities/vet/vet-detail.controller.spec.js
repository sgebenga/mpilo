'use strict';

describe('Vet Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockVet, MockSpecialty;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockVet = jasmine.createSpy('MockVet');
        MockSpecialty = jasmine.createSpy('MockSpecialty');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Vet': MockVet,
            'Specialty': MockSpecialty
        };
        createController = function() {
            $injector.get('$controller')("VetDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'mpiloApp:vetUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
