package za.co.binarylabs.web.rest;

import com.codahale.metrics.annotation.Timed;
import za.co.binarylabs.domain.PetType;
import za.co.binarylabs.repository.PetTypeRepository;
import za.co.binarylabs.repository.search.PetTypeSearchRepository;
import za.co.binarylabs.web.rest.util.HeaderUtil;
import za.co.binarylabs.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PetType.
 */
@RestController
@RequestMapping("/api")
public class PetTypeResource {

    private final Logger log = LoggerFactory.getLogger(PetTypeResource.class);
        
    @Inject
    private PetTypeRepository petTypeRepository;
    
    @Inject
    private PetTypeSearchRepository petTypeSearchRepository;
    
    /**
     * POST  /petTypes -> Create a new petType.
     */
    @RequestMapping(value = "/petTypes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PetType> createPetType(@Valid @RequestBody PetType petType) throws URISyntaxException {
        log.debug("REST request to save PetType : {}", petType);
        if (petType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("petType", "idexists", "A new petType cannot already have an ID")).body(null);
        }
        PetType result = petTypeRepository.save(petType);
        petTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/petTypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("petType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /petTypes -> Updates an existing petType.
     */
    @RequestMapping(value = "/petTypes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PetType> updatePetType(@Valid @RequestBody PetType petType) throws URISyntaxException {
        log.debug("REST request to update PetType : {}", petType);
        if (petType.getId() == null) {
            return createPetType(petType);
        }
        PetType result = petTypeRepository.save(petType);
        petTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("petType", petType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /petTypes -> get all the petTypes.
     */
    @RequestMapping(value = "/petTypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PetType>> getAllPetTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PetTypes");
        Page<PetType> page = petTypeRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/petTypes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /petTypes/:id -> get the "id" petType.
     */
    @RequestMapping(value = "/petTypes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PetType> getPetType(@PathVariable Long id) {
        log.debug("REST request to get PetType : {}", id);
        PetType petType = petTypeRepository.findOne(id);
        return Optional.ofNullable(petType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /petTypes/:id -> delete the "id" petType.
     */
    @RequestMapping(value = "/petTypes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePetType(@PathVariable Long id) {
        log.debug("REST request to delete PetType : {}", id);
        petTypeRepository.delete(id);
        petTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("petType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/petTypes/:query -> search for the petType corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/petTypes/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PetType> searchPetTypes(@PathVariable String query) {
        log.debug("REST request to search PetTypes for query {}", query);
        return StreamSupport
            .stream(petTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
