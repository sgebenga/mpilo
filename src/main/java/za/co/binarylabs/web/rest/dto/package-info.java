/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package za.co.binarylabs.web.rest.dto;
