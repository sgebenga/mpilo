package za.co.binarylabs.web.rest;

import com.codahale.metrics.annotation.Timed;
import za.co.binarylabs.domain.Specialty;
import za.co.binarylabs.repository.SpecialtyRepository;
import za.co.binarylabs.repository.search.SpecialtySearchRepository;
import za.co.binarylabs.web.rest.util.HeaderUtil;
import za.co.binarylabs.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Specialty.
 */
@RestController
@RequestMapping("/api")
public class SpecialtyResource {

    private final Logger log = LoggerFactory.getLogger(SpecialtyResource.class);
        
    @Inject
    private SpecialtyRepository specialtyRepository;
    
    @Inject
    private SpecialtySearchRepository specialtySearchRepository;
    
    /**
     * POST  /specialtys -> Create a new specialty.
     */
    @RequestMapping(value = "/specialties",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Specialty> createSpecialty(@Valid @RequestBody Specialty specialty) throws URISyntaxException {
        log.debug("REST request to save Specialty : {}", specialty);
        if (specialty.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("specialty", "idexists", "A new specialty cannot already have an ID")).body(null);
        }
        Specialty result = specialtyRepository.save(specialty);
        specialtySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/specialties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("specialty", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /specialtys -> Updates an existing specialty.
     */
    @RequestMapping(value = "/specialties",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Specialty> updateSpecialty(@Valid @RequestBody Specialty specialty) throws URISyntaxException {
        log.debug("REST request to update Specialty : {}", specialty);
        if (specialty.getId() == null) {
            return createSpecialty(specialty);
        }
        Specialty result = specialtyRepository.save(specialty);
        specialtySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("specialty", specialty.getId().toString()))
            .body(result);
    }

    /**
     * GET  /specialtys -> get all the specialtys.
     */
    @RequestMapping(value = "/specialties",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Specialty>> getAllSpecialtys(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Specialtys");
        Page<Specialty> page = specialtyRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/specialtys");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /specialtys/:id -> get the "id" specialty.
     */
    @RequestMapping(value = "/specialties/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Specialty> getSpecialty(@PathVariable Long id) {
        log.debug("REST request to get Specialty : {}", id);
        Specialty specialty = specialtyRepository.findOne(id);
        return Optional.ofNullable(specialty)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /specialtys/:id -> delete the "id" specialty.
     */
    @RequestMapping(value = "/specialties/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSpecialty(@PathVariable Long id) {
        log.debug("REST request to delete Specialty : {}", id);
        specialtyRepository.delete(id);
        specialtySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("specialty", id.toString())).build();
    }

    /**
     * SEARCH  /_search/specialtys/:query -> search for the specialty corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/specialties/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Specialty> searchSpecialtys(@PathVariable String query) {
        log.debug("REST request to search Specialtys for query {}", query);
        return StreamSupport
            .stream(specialtySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
