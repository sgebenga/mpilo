package za.co.binarylabs.web.rest;

import com.codahale.metrics.annotation.Timed;
import za.co.binarylabs.domain.Vet;
import za.co.binarylabs.repository.VetRepository;
import za.co.binarylabs.repository.search.VetSearchRepository;
import za.co.binarylabs.web.rest.util.HeaderUtil;
import za.co.binarylabs.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Vet.
 */
@RestController
@RequestMapping("/api")
public class VetResource {

    private final Logger log = LoggerFactory.getLogger(VetResource.class);
        
    @Inject
    private VetRepository vetRepository;
    
    @Inject
    private VetSearchRepository vetSearchRepository;
    
    /**
     * POST  /vets -> Create a new vet.
     */
    @RequestMapping(value = "/vets",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vet> createVet(@Valid @RequestBody Vet vet) throws URISyntaxException {
        log.debug("REST request to save Vet : {}", vet);
        if (vet.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("vet", "idexists", "A new vet cannot already have an ID")).body(null);
        }
        Vet result = vetRepository.save(vet);
        vetSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/vets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("vet", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vets -> Updates an existing vet.
     */
    @RequestMapping(value = "/vets",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vet> updateVet(@Valid @RequestBody Vet vet) throws URISyntaxException {
        log.debug("REST request to update Vet : {}", vet);
        if (vet.getId() == null) {
            return createVet(vet);
        }
        Vet result = vetRepository.save(vet);
        vetSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("vet", vet.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vets -> get all the vets.
     */
    @RequestMapping(value = "/vets",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Vet>> getAllVets(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Vets");
        Page<Vet> page = vetRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vets");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vets/:id -> get the "id" vet.
     */
    @RequestMapping(value = "/vets/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vet> getVet(@PathVariable Long id) {
        log.debug("REST request to get Vet : {}", id);
        Vet vet = vetRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(vet)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /vets/:id -> delete the "id" vet.
     */
    @RequestMapping(value = "/vets/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVet(@PathVariable Long id) {
        log.debug("REST request to delete Vet : {}", id);
        vetRepository.delete(id);
        vetSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("vet", id.toString())).build();
    }

    /**
     * SEARCH  /_search/vets/:query -> search for the vet corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/vets/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Vet> searchVets(@PathVariable String query) {
        log.debug("REST request to search Vets for query {}", query);
        return StreamSupport
            .stream(vetSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
