package za.co.binarylabs.web.rest;

import com.codahale.metrics.annotation.Timed;
import za.co.binarylabs.domain.Visit;
import za.co.binarylabs.repository.VisitRepository;
import za.co.binarylabs.repository.search.VisitSearchRepository;
import za.co.binarylabs.web.rest.util.HeaderUtil;
import za.co.binarylabs.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Visit.
 */
@RestController
@RequestMapping("/api")
public class VisitResource {

    private final Logger log = LoggerFactory.getLogger(VisitResource.class);
        
    @Inject
    private VisitRepository visitRepository;
    
    @Inject
    private VisitSearchRepository visitSearchRepository;
    
    /**
     * POST  /visits -> Create a new visit.
     */
    @RequestMapping(value = "/visits",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Visit> createVisit(@Valid @RequestBody Visit visit) throws URISyntaxException {
        log.debug("REST request to save Visit : {}", visit);
        if (visit.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("visit", "idexists", "A new visit cannot already have an ID")).body(null);
        }
        Visit result = visitRepository.save(visit);
        visitSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/visits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("visit", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /visits -> Updates an existing visit.
     */
    @RequestMapping(value = "/visits",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Visit> updateVisit(@Valid @RequestBody Visit visit) throws URISyntaxException {
        log.debug("REST request to update Visit : {}", visit);
        if (visit.getId() == null) {
            return createVisit(visit);
        }
        Visit result = visitRepository.save(visit);
        visitSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("visit", visit.getId().toString()))
            .body(result);
    }

    /**
     * GET  /visits -> get all the visits.
     */
    @RequestMapping(value = "/visits",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Visit>> getAllVisits(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Visits");
        Page<Visit> page = visitRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/visits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /visits/:id -> get the "id" visit.
     */
    @RequestMapping(value = "/visits/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Visit> getVisit(@PathVariable Long id) {
        log.debug("REST request to get Visit : {}", id);
        Visit visit = visitRepository.findOne(id);
        return Optional.ofNullable(visit)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /visits/:id -> delete the "id" visit.
     */
    @RequestMapping(value = "/visits/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVisit(@PathVariable Long id) {
        log.debug("REST request to delete Visit : {}", id);
        visitRepository.delete(id);
        visitSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("visit", id.toString())).build();
    }

    /**
     * SEARCH  /_search/visits/:query -> search for the visit corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/visits/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Visit> searchVisits(@PathVariable String query) {
        log.debug("REST request to search Visits for query {}", query);
        return StreamSupport
            .stream(visitSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
