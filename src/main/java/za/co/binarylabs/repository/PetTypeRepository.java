package za.co.binarylabs.repository;

import za.co.binarylabs.domain.PetType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PetType entity.
 */
public interface PetTypeRepository extends JpaRepository<PetType,Long> {

}
