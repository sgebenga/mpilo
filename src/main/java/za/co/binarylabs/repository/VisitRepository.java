package za.co.binarylabs.repository;

import za.co.binarylabs.domain.Visit;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Visit entity.
 */
public interface VisitRepository extends JpaRepository<Visit,Long> {

}
