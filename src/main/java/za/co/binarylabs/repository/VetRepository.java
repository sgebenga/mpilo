package za.co.binarylabs.repository;

import za.co.binarylabs.domain.Vet;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Vet entity.
 */
public interface VetRepository extends JpaRepository<Vet,Long> {

    @Query("select distinct vet from Vet vet left join fetch vet.specialtys")
    List<Vet> findAllWithEagerRelationships();

    @Query("select vet from Vet vet left join fetch vet.specialtys where vet.id =:id")
    Vet findOneWithEagerRelationships(@Param("id") Long id);

}
