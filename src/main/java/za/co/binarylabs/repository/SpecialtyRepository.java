package za.co.binarylabs.repository;

import za.co.binarylabs.domain.Specialty;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Specialty entity.
 */
public interface SpecialtyRepository extends JpaRepository<Specialty,Long> {

}
