package za.co.binarylabs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import za.co.binarylabs.domain.Pet;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Pet entity.
 */
public interface PetRepository extends JpaRepository<Pet,Long> {

    @Query("select pet from Pet pet where pet.user.login = ?#{principal.username}")
    List<Pet> findByUserIsCurrentUser();

    @Query("select pet from Pet pet where pet.user.login = ?#{principal.username}")
    Page<Pet> findByUserIsCurrentUser(Pageable pageable);


}
