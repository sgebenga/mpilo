package za.co.binarylabs.repository.search;

import za.co.binarylabs.domain.Specialty;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Specialty entity.
 */
public interface SpecialtySearchRepository extends ElasticsearchRepository<Specialty, Long> {
}
