package za.co.binarylabs.repository.search;

import za.co.binarylabs.domain.Visit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Visit entity.
 */
public interface VisitSearchRepository extends ElasticsearchRepository<Visit, Long> {
}
