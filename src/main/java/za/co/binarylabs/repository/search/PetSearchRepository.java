package za.co.binarylabs.repository.search;

import za.co.binarylabs.domain.Pet;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Pet entity.
 */
public interface PetSearchRepository extends ElasticsearchRepository<Pet, Long> {
}
