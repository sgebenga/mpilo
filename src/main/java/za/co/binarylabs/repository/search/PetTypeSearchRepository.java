package za.co.binarylabs.repository.search;

import za.co.binarylabs.domain.PetType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PetType entity.
 */
public interface PetTypeSearchRepository extends ElasticsearchRepository<PetType, Long> {
}
