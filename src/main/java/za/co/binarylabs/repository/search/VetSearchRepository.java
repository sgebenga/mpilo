package za.co.binarylabs.repository.search;

import za.co.binarylabs.domain.Vet;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Vet entity.
 */
public interface VetSearchRepository extends ElasticsearchRepository<Vet, Long> {
}
