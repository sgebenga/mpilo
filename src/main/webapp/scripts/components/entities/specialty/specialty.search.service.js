'use strict';

angular.module('mpiloApp')
    .factory('SpecialtySearch', function ($resource) {
        return $resource('api/_search/specialties/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
