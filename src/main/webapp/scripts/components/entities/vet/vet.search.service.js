'use strict';

angular.module('mpiloApp')
    .factory('VetSearch', function ($resource) {
        return $resource('api/_search/vets/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
