'use strict';

angular.module('mpiloApp')
    .factory('Vet', function ($resource, DateUtils) {
        return $resource('api/vets/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
