'use strict';

angular.module('mpiloApp')
    .factory('PetTypeSearch', function ($resource) {
        return $resource('api/_search/petTypes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
