'use strict';

angular.module('mpiloApp')
    .factory('PetType', function ($resource, DateUtils) {
        return $resource('api/petTypes/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
