'use strict';

angular.module('mpiloApp')
    .factory('VisitSearch', function ($resource) {
        return $resource('api/_search/visits/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
