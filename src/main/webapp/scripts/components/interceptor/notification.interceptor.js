 'use strict';

angular.module('mpiloApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-mpiloApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-mpiloApp-params')});
                }
                return response;
            }
        };
    });
