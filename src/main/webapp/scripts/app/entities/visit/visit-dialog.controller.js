'use strict';

angular.module('mpiloApp').controller('VisitDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Visit', 'Pet',
        function($scope, $stateParams, $uibModalInstance, entity, Visit, Pet) {

        $scope.visit = entity;
        $scope.pets = Pet.query();
        $scope.load = function(id) {
            Visit.get({id : id}, function(result) {
                $scope.visit = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('mpiloApp:visitUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.visit.id != null) {
                Visit.update($scope.visit, onSaveSuccess, onSaveError);
            } else {
                Visit.save($scope.visit, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForDate = {};

        $scope.datePickerForDate.status = {
            opened: false
        };

        $scope.datePickerForDateOpen = function($event) {
            $scope.datePickerForDate.status.opened = true;
        };
}]);
