'use strict';

angular.module('mpiloApp')
	.controller('VisitDeleteController', function($scope, $uibModalInstance, entity, Visit) {

        $scope.visit = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Visit.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
