'use strict';

angular.module('mpiloApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('visit', {
                parent: 'entity',
                url: '/visits',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'mpiloApp.visit.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/visit/visits.html',
                        controller: 'VisitController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('visit');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('visit.detail', {
                parent: 'entity',
                url: '/visit/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'mpiloApp.visit.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/visit/visit-detail.html',
                        controller: 'VisitDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('visit');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Visit', function($stateParams, Visit) {
                        return Visit.get({id : $stateParams.id});
                    }]
                }
            })
            .state('visit.new', {
                parent: 'visit',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/visit/visit-dialog.html',
                        controller: 'VisitDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    date: null,
                                    description: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('visit', null, { reload: true });
                    }, function() {
                        $state.go('visit');
                    })
                }]
            })
            .state('visit.edit', {
                parent: 'visit',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/visit/visit-dialog.html',
                        controller: 'VisitDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Visit', function(Visit) {
                                return Visit.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('visit', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('visit.delete', {
                parent: 'visit',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/visit/visit-delete-dialog.html',
                        controller: 'VisitDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Visit', function(Visit) {
                                return Visit.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('visit', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
