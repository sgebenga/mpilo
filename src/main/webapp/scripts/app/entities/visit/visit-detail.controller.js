'use strict';

angular.module('mpiloApp')
    .controller('VisitDetailController', function ($scope, $rootScope, $stateParams, entity, Visit, Pet) {
        $scope.visit = entity;
        $scope.load = function (id) {
            Visit.get({id: id}, function(result) {
                $scope.visit = result;
            });
        };
        var unsubscribe = $rootScope.$on('mpiloApp:visitUpdate', function(event, result) {
            $scope.visit = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
