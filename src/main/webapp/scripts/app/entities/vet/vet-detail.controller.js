'use strict';

angular.module('mpiloApp')
    .controller('VetDetailController', function ($scope, $rootScope, $stateParams, entity, Vet, Specialty) {
        $scope.vet = entity;
        $scope.load = function (id) {
            Vet.get({id: id}, function(result) {
                $scope.vet = result;
            });
        };
        var unsubscribe = $rootScope.$on('mpiloApp:vetUpdate', function(event, result) {
            $scope.vet = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
