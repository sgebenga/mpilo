'use strict';

angular.module('mpiloApp').controller('VetDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Vet', 'Specialty',
        function($scope, $stateParams, $uibModalInstance, entity, Vet, Specialty) {

        $scope.vet = entity;
        $scope.specialtys = Specialty.query();
        $scope.load = function(id) {
            Vet.get({id : id}, function(result) {
                $scope.vet = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('mpiloApp:vetUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.vet.id != null) {
                Vet.update($scope.vet, onSaveSuccess, onSaveError);
            } else {
                Vet.save($scope.vet, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
