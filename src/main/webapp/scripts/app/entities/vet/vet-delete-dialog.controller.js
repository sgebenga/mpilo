'use strict';

angular.module('mpiloApp')
	.controller('VetDeleteController', function($scope, $uibModalInstance, entity, Vet) {

        $scope.vet = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Vet.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
