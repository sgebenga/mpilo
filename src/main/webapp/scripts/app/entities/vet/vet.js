'use strict';

angular.module('mpiloApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('vet', {
                parent: 'entity',
                url: '/vets',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'mpiloApp.vet.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vet/vets.html',
                        controller: 'VetController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('vet');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('vet.detail', {
                parent: 'entity',
                url: '/vet/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'mpiloApp.vet.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vet/vet-detail.html',
                        controller: 'VetDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('vet');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Vet', function($stateParams, Vet) {
                        return Vet.get({id : $stateParams.id});
                    }]
                }
            })
            .state('vet.new', {
                parent: 'vet',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/vet/vet-dialog.html',
                        controller: 'VetDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    firstName: null,
                                    lastName: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('vet', null, { reload: true });
                    }, function() {
                        $state.go('vet');
                    })
                }]
            })
            .state('vet.edit', {
                parent: 'vet',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/vet/vet-dialog.html',
                        controller: 'VetDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Vet', function(Vet) {
                                return Vet.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('vet', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('vet.delete', {
                parent: 'vet',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/vet/vet-delete-dialog.html',
                        controller: 'VetDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Vet', function(Vet) {
                                return Vet.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('vet', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
