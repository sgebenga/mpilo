'use strict';

angular.module('mpiloApp').controller('PetTypeDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'PetType',
        function($scope, $stateParams, $uibModalInstance, entity, PetType) {

        $scope.petType = entity;
        $scope.load = function(id) {
            PetType.get({id : id}, function(result) {
                $scope.petType = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('mpiloApp:petTypeUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.petType.id != null) {
                PetType.update($scope.petType, onSaveSuccess, onSaveError);
            } else {
                PetType.save($scope.petType, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
