'use strict';

angular.module('mpiloApp')
	.controller('PetTypeDeleteController', function($scope, $uibModalInstance, entity, PetType) {

        $scope.petType = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PetType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
