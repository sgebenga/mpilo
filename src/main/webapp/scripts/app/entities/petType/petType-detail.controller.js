'use strict';

angular.module('mpiloApp')
    .controller('PetTypeDetailController', function ($scope, $rootScope, $stateParams, entity, PetType) {
        $scope.petType = entity;
        $scope.load = function (id) {
            PetType.get({id: id}, function(result) {
                $scope.petType = result;
            });
        };
        var unsubscribe = $rootScope.$on('mpiloApp:petTypeUpdate', function(event, result) {
            $scope.petType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
