'use strict';

angular.module('mpiloApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('petType', {
                parent: 'entity',
                url: '/petTypes',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'mpiloApp.petType.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/petType/petTypes.html',
                        controller: 'PetTypeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('petType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('petType.detail', {
                parent: 'entity',
                url: '/petType/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'mpiloApp.petType.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/petType/petType-detail.html',
                        controller: 'PetTypeDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('petType');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'PetType', function($stateParams, PetType) {
                        return PetType.get({id : $stateParams.id});
                    }]
                }
            })
            .state('petType.new', {
                parent: 'petType',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/petType/petType-dialog.html',
                        controller: 'PetTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('petType', null, { reload: true });
                    }, function() {
                        $state.go('petType');
                    })
                }]
            })
            .state('petType.edit', {
                parent: 'petType',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/petType/petType-dialog.html',
                        controller: 'PetTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['PetType', function(PetType) {
                                return PetType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('petType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('petType.delete', {
                parent: 'petType',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/petType/petType-delete-dialog.html',
                        controller: 'PetTypeDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['PetType', function(PetType) {
                                return PetType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('petType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
