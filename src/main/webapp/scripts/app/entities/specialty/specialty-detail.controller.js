'use strict';

angular.module('mpiloApp')
    .controller('SpecialtyDetailController', function ($scope, $rootScope, $stateParams, entity, Specialty) {
        $scope.specialty = entity;
        $scope.load = function (id) {
            Specialty.get({id: id}, function(result) {
                $scope.specialty = result;
            });
        };
        var unsubscribe = $rootScope.$on('mpiloApp:specialtyUpdate', function(event, result) {
            $scope.specialty = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
