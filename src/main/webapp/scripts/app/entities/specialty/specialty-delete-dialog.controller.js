'use strict';

angular.module('mpiloApp')
	.controller('SpecialtyDeleteController', function($scope, $uibModalInstance, entity, Specialty) {

        $scope.specialty = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Specialty.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
