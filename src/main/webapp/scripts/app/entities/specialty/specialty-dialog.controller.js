'use strict';

angular.module('mpiloApp').controller('SpecialtyDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Specialty',
        function($scope, $stateParams, $uibModalInstance, entity, Specialty) {

        $scope.specialty = entity;
        $scope.load = function(id) {
            Specialty.get({id : id}, function(result) {
                $scope.specialty = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('mpiloApp:specialtyUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.specialty.id != null) {
                Specialty.update($scope.specialty, onSaveSuccess, onSaveError);
            } else {
                Specialty.save($scope.specialty, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
