'use strict';

angular.module('mpiloApp').controller('PetDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Pet', 'PetType', 'User', 'Visit',
        function($scope, $stateParams, $uibModalInstance, entity, Pet, PetType, User, Visit) {

        $scope.pet = entity;
        $scope.pettypes = PetType.query();
        $scope.users = User.query();
        $scope.visits = Visit.query();
        $scope.load = function(id) {
            Pet.get({id : id}, function(result) {
                $scope.pet = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('mpiloApp:petUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.pet.id != null) {
                Pet.update($scope.pet, onSaveSuccess, onSaveError);
            } else {
                Pet.save($scope.pet, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForBirthDate = {};

        $scope.datePickerForBirthDate.status = {
            opened: false
        };

        $scope.datePickerForBirthDateOpen = function($event) {
            $scope.datePickerForBirthDate.status.opened = true;
        };
}]);
