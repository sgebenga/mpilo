'use strict';

angular.module('mpiloApp')
    .controller('PetDetailController', function ($scope, $rootScope, $stateParams, entity, Pet, PetType, User, Visit) {
        $scope.pet = entity;
        $scope.load = function (id) {
            Pet.get({id: id}, function(result) {
                $scope.pet = result;
            });
        };
        var unsubscribe = $rootScope.$on('mpiloApp:petUpdate', function(event, result) {
            $scope.pet = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
